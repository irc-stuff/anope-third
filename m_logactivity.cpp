/*
 *
 * (C) 2023 CrazyCat
 * irc.zeolia.chat
 *
 */

#include "module.h"
#include "modules/sql.h"

class CommandCSSetLogchan : public Command
{
   public:
      CommandCSSetLogchan(Module *creator) : Command(creator, "chanserv/set/logchan", 2, 2)
      {
         this->SetDesc(_("Turn channel logging on or off"));
         this->SetSyntax(_("\037channel\037 (ON | OFF)"));
      }
      
      void Execute(CommandSource &source, const std::vector<Anope::string> &params) anope_override
      {
         ChannelInfo *ci = ChannelInfo::Find(params[0]);
         if (!ci)
         {
            source.Reply(CHAN_X_NOT_REGISTERED, params[0].c_str());
            return;
         }
         EventReturn MOD_RESULT;
         FOREACH_RESULT(OnSetChannelOption, MOD_RESULT, (source, this, ci, params[1]));
         if (MOD_RESULT == EVENT_STOP)
            return;

         if (MOD_RESULT != EVENT_ALLOW && !source.AccessFor(ci).HasPriv("SET") && source.permission.empty() && !source.HasPriv("chanserv/administration"))
         {
            source.Reply(ACCESS_DENIED);
            return;
         }
         if (params[1].equals_ci("ON"))
         {
            ci->Extend<bool>("CS_LOGCHAN");
            source.Reply(_("Channel logger is now enabled for this channel."));
            Log(source.AccessFor(ci).HasPriv("SET") ? LOG_COMMAND : LOG_OVERRIDE, source, this, ci) << "to enable logchan";
         }
         else if (params[1].equals_ci("OFF"))
         {
            Log(source.AccessFor(ci).HasPriv("SET") ? LOG_COMMAND : LOG_OVERRIDE, source, this, ci) << "to disable logchan";
            ci->Shrink<bool>("CS_LOGCHAN");
            source.Reply(_("Channel logger is now disabled for this channel."));
         }
         else
            this->OnSyntaxError(source, "");
      }
      bool OnHelp(CommandSource &source, const Anope::string &) anope_override
      {
         this->SendSyntax(source);
         source.Reply(" ");
         source.Reply(_("Turns channel logger ON or OFF."));
         return true;
      }
};

/**
class CommandCSSASetLogchan: public CommandCSSetLogchan
{
 public:
	CommandCSSASetLogchan(Module *creator) : Command(creator, "chanserv/saset/logchan", 2)
	{
		this->ClearSyntax();
		this->SetSyntax(_("\037channel\037 {ON | OFF}"));
	}

	void Execute(CommandSource &source, const std::vector<Anope::string> &params) anope_override
	{
		this->Run(source, params[0], params[1], true);
	}

	bool OnHelp(CommandSource &source, const Anope::string &) anope_override
	{
		this->SendSyntax(source);
		source.Reply(" ");
		source.Reply(_("Turns channel logger ON or OFF for this channel."));
		return true;
	}
};
*/
class MySQLInterface : public SQL::Interface
{
 public:
	MySQLInterface(Module *o) : SQL::Interface(o) { }

	void OnResult(const SQL::Result &r) anope_override
	{
	}

	void OnError(const SQL::Result &r) anope_override
	{
		if (!r.GetQuery().query.empty())
			Log(LOG_DEBUG) << "Chanlogger: Error executing query " << r.finished_query << ": " << r.GetError();
		else
			Log(LOG_DEBUG) << "Chanlogger: Error executing query: " << r.GetError();
	}
};

class MLogchan : public Module
{
   SerializableExtensibleItem<bool> m_logchan;
   
   ServiceReference<SQL::Provider> sql;
	MySQLInterface sqlinterface;
	SQL::Query query;
   Anope::string prefix, mtype;
   std::vector<Anope::string> Logtable;
   
   void RunQuery(const SQL::Query &q)
	{
		if (sql)
			sql->Run(&sqlinterface, q);
	}
   
   const Anope::string GetDisplay(User *u)
	{
		if (u && u->Account())
			return u->Account()->display;
		else
			return u->nick;
	}
   
   void GetTables()
	{
		Logtable.clear();
		if (!sql)
			return;

		SQL::Result r = this->sql->RunQuery(this->sql->GetTables(prefix));
		for (int i = 0; i < r.Rows(); ++i)
		{
			const std::map<Anope::string, Anope::string> &map = r.Row(i);
			for (std::map<Anope::string, Anope::string>::const_iterator it = map.begin(); it != map.end(); ++it)
				Logtable.push_back(it->second);
		}
	}

   bool HasTable(const Anope::string &table)
	{
		for (std::vector<Anope::string>::const_iterator it = Logtable.begin(); it != Logtable.end(); ++it)
			if (*it == table)
				return true;
		return false;
	}
   
   void MakeQuery(Channel *c, const Anope::string &nick, const Anope::string &ltype, const Anope::string &content, const Anope::string &target)
   {
       query = "INSERT INTO `" + prefix + "chanlog` (chan, nick, type, content, target) VALUES (@chan@, @nick@, @type@, @content@, @target@);";
       query.SetValue("chan", c->name);
       query.SetValue("nick", nick);
       query.SetValue("type", ltype);
       query.SetValue("content", content);
       query.SetValue("target", target);
       this->RunQuery(query);
   }
   
   void CheckTables()
	{
		this->GetTables();
		if (!this->HasTable(prefix +"chanlog"))
		{
			query = "CREATE TABLE `" + prefix + "chanlog` ("
				"`chan` varchar(64) NOT NULL DEFAULT '',"
            " `ts` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,"
				" `nick` varchar(64) NOT NULL DEFAULT '',"
            " `type` VARCHAR(20) NOT NULL,"
            " `content` TEXT NOT NULL DEFAULT '',"
            " `target` varchar(64) NOT NULL DEFAULT '',"
				" KEY `chan` (`chan`),"
				" KEY `nick` (`nick`),"
				" KEY `type` (`type`)"
				") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;";
			this->RunQuery(query);
		}
   }
   
   public:
	MLogchan(const Anope::string &modname, const Anope::string &creator) :
		Module(modname, creator, THIRD),
		m_logchan(this, "BS_STATS"),
		sqlinterface(this)
	{
      this->SetAuthor("CrazyCat <crazycat@c-p-f.org>");
      this->SetVersion("0.0.1");
	}

	void OnReload(Configuration::Conf *conf) anope_override
	{
		Configuration::Block *block = conf->GetModule(this);
		prefix = block->Get<const Anope::string>("prefix", "anope_");
		Anope::string engine = block->Get<const Anope::string>("engine");
		this->sql = ServiceReference<SQL::Provider>("SQL::Provider", engine);
		if (sql)
			this->CheckTables();
		else
			Log(this) << "no database connection to " << engine;
	}
   
   void OnPrivmsg(User *u, Channel *c, Anope::string &msg) anope_override
	{
      if (!c->ci || !m_logchan.HasExt(c->ci))
         return;
      if (msg.find("\01ACTION")!=Anope::string::npos)
         mtype = "ACTION";
      else
         mtype = "PUB";
      this->MakeQuery(c, GetDisplay(u), mtype, msg, "");
   }
   
   void OnJoinChannel(User *u, Channel *c) anope_override
   {
      if (!c->ci || !m_logchan.HasExt(c->ci))
         return;
      this->MakeQuery(c, GetDisplay(u), "JOIN", "", "");
   }
   
   void OnPartChannel(User *u, Channel *c, Anope::string &channel, Anope::string &msg) anope_override
   {
      if (!c->ci || !m_logchan.HasExt(c->ci))
         return;
      this->MakeQuery(c, GetDisplay(u), "PART", msg, "");
   }
};

MODULE_INIT(MLogchan)